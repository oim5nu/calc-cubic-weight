const axios = require('axios');

async function getProduct(endpoint) {
  let payload = null;
  try {
    const response = await axios.get(endpoint);
    const { data } = response;
    payload = data;
  } catch (error) {
    console.error(error);
  } 
  return payload;
}

async function findProductsByCategory(baseUrl='', nextApi='', category='') {
  let endpoint = `${baseUrl}${nextApi}`;
  let targetProducts = [];
  
  do {
    let data;
    try {
      data = await getProduct(endpoint);
    } catch(error) {
      console.error(error);
    } 

    let { objects, next } = data || {};
    objects = objects || [];

    let products = objects.filter(obj => obj.category === category);
    targetProducts = [...targetProducts, ...products];

    nextApi = next || null;
    endpoint = `${baseUrl}${nextApi}`;
  } while(nextApi != null)

  return targetProducts;
}

function calculateAverateCubicWeight(products=[], conversionFactor=250) {
  // validation for products and conversionFactor, 
  // may need extra detail validation for products
  if (Array.isArray(products) === false ) {
    console.log('Input products must be array');
    return null;
  }
  if (products.length === 0) {
    console.log('No products found in array');
    return null;
  }
  if ( typeof conversionFactor !== 'number' ) {
    console.log('conversionFactor must be number');
    return null;
  }
  const cubicWeights = products.map(product => {
    const { size: { width, length, height } } = product;
    return width * length * height / 1000000.00 * conversionFactor;
  })
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  return cubicWeights.reduce(reducer) / cubicWeights.length;
}

module.exports = {
  getProduct,
  findProductsByCategory,
  calculateAverateCubicWeight
};