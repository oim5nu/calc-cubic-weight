# Installation & Run Instructions

## Requirements: node.js 8.x.x or above and git under windows/linux/mac

## Steps:
1. Open a terminal, execute command line, git clone https://oim5nu@bitbucket.org/oim5nu/calc-cubic-weight.git
2. cd calc-cubic-weight
3. npm install
4. npm start