let expect = require('chai').expect;
//const nock = require('nock');
//const { response } = require('./response');
const { findProductsByCategory, getProduct, calculateAverateCubicWeight } = require('../cubic_weight');

describe('First Test', function() {
  it('[1,2,3] should have 3 elements', function() {
    expect([1,2,3]).to.have.lengthOf(3);
  });
});

describe('#getProduct()', function() {
  // beforeEach(() => {
  //   nock('http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com/')
  //     .get('/api/products/1')
  //     .reply(200, response);
  // });

  const endpoint = 'http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com/api/products/1';
  context('with valid uri', function() {
    it('should return data with attribute objects and next', async function() {
      const data = await getProduct(endpoint);
      expect(typeof data).to.equal('object');
      expect(data).to.have.property('objects');
      expect(data).to.have.property('next');
    })
  })

  context('with invalid uri', function() {
    const invalidUri = 'http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com/api/products';
    it('should return null', async function() {
      const data = await getProduct(invalidUri);
      expect(data).to.be.null;
    })
  })
});
