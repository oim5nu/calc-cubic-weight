const { findProductsByCategory, calculateAverateCubicWeight } = require('./cubic_weight');

async function run() {
  let nextApi = '/api/products/1';
  const baseUrl = 'http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com';
  const category = 'Air Conditioners';
  const products = await findProductsByCategory(baseUrl, nextApi, category);
  const coversionFactor = 250;
  const averageCubicWeight = calculateAverateCubicWeight(products, coversionFactor);
  console.log('averageCubicWeight:', averageCubicWeight);
}

run();